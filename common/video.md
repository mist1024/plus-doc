# 视频教程(联合出品)

### 主讲与后期剪辑: `抓蛙师` 

抓蛙师简介: B站知名UP主 B站首页: https://space.bilibili.com/520725002

### 知识点统筹与内容审核: `疯狂的狮子Li`

疯狂的狮子Li简介: RuoYi-Vue-Plus 与 RuoYi-Cloud-Plus 作者

## 开售日期: 6月21日开售 优惠价: 598(仅限前500名) ~~原价: 698~~

> 视频采用 Vue 版本 4.X 分支讲解 (内容为通用技术与版本关联性不大)
>
> 内容为框架内所用到的技术与设计原理(打破不知道、不会用、不知应用场景等问题)

课程简介: https://www.bilibili.com/video/BV16j411D7BX/
<br>
试看课程: https://www.bilibili.com/video/BV1xV4y127KM/
<br>
**小本生意 用心录制 拒绝砍价 目前已更新到 147 集**<br>
课程咨询群: QQ群号: **<866407661>** 进群后联系管理员通过微信或支付宝付款即可<br>
课程咨询群: QQ群号: **<866407661>** 进群后联系管理员通过微信或支付宝付款即可<br>
课程咨询群: QQ群号: **<866407661>** 进群后联系管理员通过微信或支付宝付款即可<br>

## 购买前常见问题答疑
> 问题1: 购买后是否有群可以解答问题<br>
> 答: 购买后有专属课程付费群讲师在线答疑
> 
> 问题2: 是否持续更新 如新版本功能<br>
> 答: 课程目录即为全部课程内容 以课程目录为准 明年大概会出二期来讲新版本内容<br>
> 因为持续更新会导致前面的技术老旧 新购买的人无法及时学习新技术<br>
> 故而采用分期出课程制度 已经购买过的老客户 再次购买下一次会给力度非常大的折扣
> 
> 问题3: 目前视频未全部录制完成 后续更新是否二次收费<br>
> 答: 视频目录即为全部视频内容 一次收费后续更新仍然可看直到视频全部更新完成(明年出二期课程不算在内)
>
> 问题4: 视频如何下载如何观看<br>
> 答: 视频文件已加密 采用专门的播放器(播放器只限制截图录屏等不限制其他软件使用) 由管理员发放授权码观看<br>
> 支持通过 百度云 或者 阿里云 网盘下载视频资源
>
> 问题5: 视频平均时长和总时长大概多久<br>
> 答: 视频每集短的大概10分钟以上 长的大概40个分钟左右 平均时长20多分钟每集<br>
> 目前已经录制了120集总时长为40多个小时 预计总时长70多个小时
> 
> 问题6: 是否有讲解 Cloud 版本相关内容<br>
> 答: 视频主要讲解内容为框架内所用到的技术与设计原理 无论什么版本 功能和设计都是一样的<br>
> Cloud 版本只是多了 alibaba 的几个组件完全可以B站自学

## 课程目录
```
1. 课程介绍
2. 开发环境搭建（Git、Java、Mysql、Redis及管理工具）
3. 开发环境搭建（Minio、Maven、Idea安装和设置）
4. 项目下载运行
5. 数据库文件的介绍
6. 后端项目结构
7. 前端项目结构
8. 系统管理-用户管理
9. 系统管理-个人中心和页面布局
10. 系统管理-角色管理
11. 系统管理-菜单管理
12. 系统管理-部门管理
13. 系统管理-岗位管理
14. 系统管理-字典管理
15. 系统管理-参数设置
16. 系统管理-通知公告
17. 系统管理-日志管理(操作日志、登录日志)
18. 系统管理-文件管理(文件上传、配置管理)
19. 系统监控-在线用户
20. 系统监控-缓存监控
21. 系统监控-Admin监控
22. 系统监控-任务调度中心
23. 系统监控-缓存列表
24. 系统工具-表单构建
25. 系统工具-代码生成(单表)
26. 系统工具-单表代码生成演示
27. 系统工具-代码生成(树表)
28. 系统工具-树表代码生成演示
29. maven配置讲解(pom文件)
30. maven项目新增模块
31. application.yml配置讲解
32. jackson 配置讲解(序列化配置)
33. JsonUtils 工具用法讲解
34. mybatis映射文件标签的使用
35. mybatisSQL语句抽离及resultMap
36. mybatis insert update delete标签
37. BeanCopyUtils工具用法讲解
38. mybatis-plus 介绍
39. mybatis-plus 自动审计(元数据)
40. mybatis-plus 逻辑删除、乐观锁
41. mybatis-plus BaseMapperPlus
42. mybatis-plus 分布式主键-雪花ID
43. mybatis-plus 基础增删改查
44. mybatis-plus 条件构造器 Wrapper
45. mybatis-plus 条件构造器 方法示例1（eq between apply last func nested clear）
46. mybatis-plus 条件构造器 方法示例2（having order or clone segment lambda）
47. mybatis-plus 新增、批量新增、根据条件修改、删除
48. mybatis-plus 分页查询（TableDataInfo、PageQuery）
49. redis 介绍
50. redisson 介绍与配置(单机、集群、序列化配置、其他连接方式扩展)
51. RedisUtils工具类讲解（一）
52. RedisUtils工具类讲解（二）
53. sa-token 介绍
54. sa-token 配置简介
55. sa-token jwt 介绍
56. 框架登录流程说明
57. 验证码生成接口源码
58. permission.js路由守卫和request.js请求封装
59. login 登录接口源码分析
60. LoginHelper功能分析与登录密码匹配逻辑
61. LoginHelper登录功能深入底层源码讲解
62. 请求接口token认证逻辑与拦截器讲解
63. getInfo接口获取当前用户信息讲解
64. 全局异常拦截器讲解
65. logout接口系统登出源码讲解
66. router接口获取系统路由菜单讲解
67. 路由生成处理逻辑router.js与permission.js源码讲解
68. 侧边栏菜单和顶部菜单渲染过程源码讲解
69. 前端按钮权限控制与后端权限获取(自定义指令)
70. sa-token 权限拦截器 SaInterceptor 源码讲解
71. sa-token @SaCheckLogin 注解校验逻辑
72. sa-token @SaCheckPermission、@SaCheckRole 注解校验逻辑
73. sa-token @SaCheckSafe 二级认证 用法与校验逻辑
74. sa-token @SaCheckDisable 账号封禁 用法与校验逻辑
75. sa-token @SaCheckBasic HttpBasic认证 用法与校验逻辑
76. 角色部门-数据权限使用讲解
77. 角色部门-类上数据权限注解及自定义数据权限讲解
78. 角色部门-数据权限整体梳理实现过程
79. 玩转Spring中强大的spel表达式!(原理、字面量、基本运算)
80. 玩转Spring中强大的spel表达式!(逻辑运算、类相关表达式、自定义函数、Bean引用)
81. 玩转Spring中强大的spel表达式!(集合相关表达式和模板表达式)
82. 角色部门-数据权限处理器源码分析
83. EasyExcel的使用（ExcelUtil工具类）（一）
84. EasyExcel的使用（ExcelUtil工具类）（二）
85. EasyExcel相关类解读
86. EasyExcel的应用（用户管理-导入用户-导出用户）
87. SpringCache+CacheManager介绍和@Cacheable注解详解
88. SpringCache注解使用方式详解
89. SpringCache实现过程源码解读
90. CacheUtils缓存工具类
91. 缓存穿透、缓存击穿、缓存雪崩解决方案与布隆过滤器
92. echarts图表介绍与使用
93. 缓存监控-redis状态管理
94. 缓存列表-redis key管理
95. Logback日志框架介绍和@Slf4j注解及日志方法调用
96. Logback日志框架配置logback-plus.xml详解
97. p6spy sql性能分析(sql打印)
98. SpringEvent事件监听
99. 登录日志源码讲解
100. 日志注解@Log和操作日志源码讲解
101. 线程池核心流程图解
102. 线程池参数说明、基本原理与配置和使用方式详解
103. @Async注解(异步调用的讲解)
104. aws-s3简介与OSS模块详解(文件上传的核心流程)
105. 文件上传-前端上传组件
106. 文件上传-阿里云
107. 文件上传-腾讯云
108. 文件上传-七牛云
109. velocity模板语法和修改
110. 代码生成原理及流程
111. 代码生成-查询可导入表与导入表的初始化
112. 代码生成-查询导入的表信息与修改表代码生成配置
113. 代码生成-预览代码
114. 代码生成-自定义路径、生成压缩包与同步数据库字段
115. HikariCP连接池性能测试与配置
116. dynamic-datasource多数据源的配置和基本使用
117. 多数据源-请求头传值实现数据源切换与@DS注解的更多用法
118. 多数据源-DynamicDataSourceContextHolder手动切换数据源
119. 多数据源-@DSTransactional解决多数据源事务问题
120. 多数据源-通过构造Interceptor拦截器动态切换数据源
121. 多数据源-动态添加移除数据源与DataBaseHelper工具类的使用
122. 任务调度中心-前置知识内置@Scheduled注解实现与xxl-job介绍
123. 任务调度中心-配置管理与基础调度示例
124. 任务调度中心-轮询策略和分片广播策略
125. 任务调度中心-命令行任务、跨平台Http任务与任务生命周期、告警配置
126. 监控中心-配置和运行
127. 监控中心-应用监控
128. common模块公共实体、常量、枚举等介绍使用
129. hutool介绍与文档使用
130. ServletUtils 客户端工具
131. SpringUtils Spring工具
132. StreamUtils 流处理工具
133. StringUtils 字符串工具
134. TreeBuildUtils 树工具
135. ReflectUtils 反射工具
136. SqlUtil sql工具
137. RegionUtils与AddressUtils工具讲解
138. MessageUtils 国际化工具(国际化配置)
139. Validator 校验框架配置与注解、ValidatorUtils工具使用
140. undertow对比tomcat与jmeter压力测试工具使用
141. XSS安全防护
142. requestBody 缓存配置(可重复读)
143. openapi 介绍与文档生成
144. SpringDoc 注解生成文档
145. javadoc 注释生成文档
146. apifox 接口文档工具
147. 使用apifox管理项目api
148. QueueUtils 队列工具
149. 普通队列(订阅队列) 有界队列
150. 延迟队列 优先队列
151. 防重幂等功能使用
152. 防重幂等设计(美团GTIS系统)
153. 限流功能使用(全局、IP、集群实例限流)
154. 短信功能配置与使用
155. 邮件功能配置与使用
156. 翻译模块讲解(三种模式)
157. 翻译用户名、字典、ossId转url、对象翻译
158. 数据脱敏讲解(内置脱敏策略)
159. 数据加解密-配置与用法
160. 数据加解密-mybatis拦截器
161. 单元测试 JUnit5、注解与断言
162. 单元测试 基于springboot环境
163. 单元测试 带参数单元测试
164. 单元测试 标签单元测试
165. ElementUI介绍和使用
166. ES6语法介绍
167. 前端App.Vue和main.js配置
168. 接口封装和使用
169. Vuex的模块封装和使用
170. directive自定义指令使用
171. plugins插件封装和使用
172. utils工具-dict字典工具
173. utils工具-其他工具的使用
174. 前端页面生命周期
175. 前端组件的封装和引用
176. Breadcrumb面包屑组件
177. DictData字典数据组件
178. DictTag字典标签组件
179. Editor富文本编辑组件
180. FileUpload文件上传组件
181. Hamburger汉堡包组件
182. HeaderSearch顶部搜索组件
183. IconSelect图标选择组件
184. iFrame内置框架组件
185. ImagePreview图片预览组件
186. ImageUpload图片上传组件
187. Pagination分页组件
188. PanThumb圆形缩略图组件
189. ParentView组件
190. RightPanel右侧设置面板组件
191. RightToolbar右侧工具条组件
192. Screenfull全屏组件
193. SizeSelect字体大小选择组件
194. SvgIcon svg图标组件
195. ThemePicker主题选择组件
196. TopNav顶部菜单组件
197. layout公共页面相关组件
198. 部门树组件
199. plugins插件使用与封装
200. Vuex模块使用与封装
201. utils工具类index.js讲解
202. utils工具类ruoyi.js讲解
203. utils工具类validate.js讲解
204. 表单构建原理
205. 后端打包发布运行
206. nginx配置（反向代理）
207. 前端打包发布
208. docker介绍与原理讲解
209. docker安装与使用
210. dockerfile语法与构建镜像
211. docker-compose介绍与安装
212. docker-compose语法与使用
213. 一键编排启动
214. IDEA的docker插件使用
215. 服务日志的监控
216. 拓展-如何搭建C端应用（uniapp）
217. 拓展-多设备多用户设计
218. 拓展-移动端用户的登录和鉴权（小程序端）
219. 拓展-移动端用户的登录和鉴权（公众号端）
220. 拓展-移动端用户的登录和鉴权（手机号）
221. 拓展-移动端简单业务开发
```

## 学员观后感

|                                                                                                                            |                                                                                                                            |
|----------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------|
| ![输入图片说明](https://foruda.gitee.com/images/1691386100129796781/44b69dae_1766278.jpeg "e5932bdbfd5eb22b764f77da2a090ed.jpg") | ![输入图片说明](https://foruda.gitee.com/images/1691386076834242484/a6073f7d_1766278.png "6257f457af8fffaadaf1358ccaa5627.png")  |
| ![输入图片说明](https://foruda.gitee.com/images/1691386089186649583/98ac8b7c_1766278.png "b695e072286e892497a225bf6209fac.png")  | ![输入图片说明](https://foruda.gitee.com/images/1691386108722171132/b937b23a_1766278.jpeg "f1a245e88f7233000fbbe28df63be14.jpg") |